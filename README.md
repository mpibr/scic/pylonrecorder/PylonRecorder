# Description
Standalone acquisition and recording software for Basler Cameras using the PylonSDK
Also offers basic support of USB webcams through OpenCV

# Required libraries
Uses QT OpenCV and the Pylon SDK (included in Release)
Requires the (free) Pylon5 libraries which come with PylonViewer
Download from the [Basler website] (http://www.baslerweb.com/en/products/software/pylon-windows)


**IMPORTANT**: The program itself does not allow adjustments of camera parameters.
Use PylonViewer to adjust all settings and look at Con

Compiles on Windows 7-10 64bit (MSVC2013) and Linux (gcc).
The Pylon SDK is currently not available on OSX.

# Usage
The program is released as one executable file ("PylonRecorder") together 
with all dynamically linked libraries in the same folder.
On startup the program looks for the first available Basler camera. 
If multiple cameras are present it will open the first one which has not 
been opened. This means that multiple instances of the program can run 
accessing multiple devices.
When a camera was opened the camera is directly started in continuous mode
for live view.

## Modes
- Framewise Trigger: A single frame is acquired on each trigger.

- Continuous: An infinite number of frames is acquired on trigger 
until the user clicks "Stop".

- Burst: A given number of frames is acquired. When the desired number
of frames was entered the program displays the resulting length of the
recording (hh:mm:ss:ms).

## Filename
A filename can be entered or picked with the browse button. If the "auto"
checkbox is ticked the filename is automatically generated from the timeStamp
of the first frame that was acquired. The browse button can still be used
when "auto" is checked to browse for a folder to store the automatically
named files.

## Trigger
The recording can be triggered by a software trigger ("Trigger" button) or
through a hardware trigger if the "External Trigger" checkbox is 
checked. Make sure that the line mode is set to input for the trigger line!
E.g. Line Selector -> Line 4, Line Mode -> Input (in PylonViewer)
The first available line is taken for external trigger.

## External stop
Using the command line parameter `--stopline <line>` (e.g. `--stopline 3`),
a line can be used to externally stop the recording. If activated, the recording is stopped when the specified line
toggles from low to high. This behavior can be reversed by inverting the line
in the configuration file or through pylon Viewer.
The signal can only be read as long as frames are incoming. 
Hence if the camera is not acquiring frames, the signal can also not be read.

## User defined output (e.g. through plugins)
Plugins can use PylonRecorder to output TTL signals on the general purpose lines. For this to work, the
line has to be set as output and has to have the User Output signal assigned.
E.g. Digital I/O Control: Line Selector -> Line 4, Line Mode -> Output, Line Source -> User Output 3) (in PylonViewer)

or 

Digital I/O Control: Line Selector -> Line 3, Line Mode -> Output, Line Source -> User Output 2) (in PylonViewer)

**Important: Currently only the direct-coupled lines (3 & 4) can be used from PylonRecorder.**

## Command line options

Options can be added using the `--<option>` syntax

| Option (long) | Option (short) | Description |
| --- | --- | --- |
| `window-x` | `wndx` | Open the programs window at x (pixel) |
| `window-y` | `wndy` | Open the programs window at y (pixel) |
| `window-width` | `wndw` | Set the programs window width to (pixel) |
| `window-height` | `wndh` | Set the programs window height to (pixel) |
| `path` | `p` | Set the programs path to (directory) |
| `display-framerate` | `df` | Set the programs display framerate (framerate) |
| `camera-idx` | `cam` | Try to use camera (idx) |
| `config-file` | `cfg` | Load or save configuration from/to (file) (load if (file) exists) |
| `recording-codec` | `codec` | Record using (codec) |
| `compression-quality` | `compression` | Set compression quality of codec to <value> (0-100 |
| `autofilename` | | Enable automated naming of files on startup |
| `stopline` | | Enable hardware recoding stop on first available input line |
| `record` | | Enable recording on startup |
| `external` | | Enable external triggering on startup |
| `continuous` | | Start camera in continuous mode |
| `framewise` | | Start camera in framewise mode |
| `burst` | | Start camera in burst mode |
| `frames` | | Record number of <frames> |

Example: `PylonRecorder.exe --wndx 10 --wndy 30 --wndw 400 --wndh 400 --p D:\test --df 10 --cam 0`

Currently supported videocodecs are: h264 (default), mjpeg or none (raw)

The Windows release also includes a startup.bat file to which command line options can be added.
startup.vbs links to that same file and can be used to create a shortcut to the Desktop that prevents
the batch file from opening an addditional Command shell window.

## Configuration files

### Basler cameras

The settings of Basler cameras are stored on the camera itself. You can use 
**PylonViewer** to directly change the configurations on the camera.

Alternatively you can load a .pfs settings file on to the camera by using **PylonRecorder** and the command line parameter --cfg <filename> or --config-file <filename>
If *filename* exists it will be loaded, otherwise the program will write a file containing the current configuration of the camera.

**PylonViewer**  can also save a .pfs configuration file (Camera -> Save Features).

You can also adjust the settings by modifying an existing .pfs settings file (in any text
editor).

### USB cameras

The program also offers basic support for USB cameras through OpenCV. Supported cameras can be configured through a configuration file that contains the opencv configuration parameters.
Currently the following parameters are supported.

| Parameter | Meaning |
| --- | ---|
| `CV_CAP_PROP_FRAME_WIDTH` | Frame width |
| `CV_CAP_PROP_FRAME_HEIGHT` | Frame height|
| `CV_CAP_PROP_FPS` | Framerate |
| `CV_CAP_PROP_FOURCC` | Codec |
| `CV_CAP_PROP_BRIGHTNESS` |Brightness|
| `CV_CAP_PROP_CONTRAST` |Contrast
| `CV_CAP_PROP_SATURATION` |Saturation|
| `CV_CAP_PROP_HUE` |Hue| 
| `CV_CAP_PROP_GAIN` |Gain|
| `CV_CAP_PROP_EXPOSURE` |Exposure|

The configuration file should contain one parameter per line followed by a comma and the value it should be set to.
E.g. A configuration file to set the camera resolution would contain:

`CV_CAP_PROP_FRAME_WIDTH`, 640

`CV_CAP_PROP_FRAME_HEIGHT`, 320


## Plugins
To use one of the available tracking plugins for PylonRecorder you need
to download and place the plugin's shared library (on windows a .dll file) 
into the PylonRecorder program folder. PylonRecorder will then 
automatically detect and load the plugin.

## Socket
The program listens on port 20001 for TCP connections. Once a client 
connected, parameters are sent to the client in the following packet 
format: 

1. uint32 packetSize;
2. uint32 packetType
3. data

packetSize is the size of the TCP packet in bytes.
PacketType can be a value from the following list:

* SOCKET_TIMESTAMP 0
* SOCKET_START 1
* SOCKET_STOP 2
* SOCKET_FILENAME 3

In case of SOCKET_START, SOCKET_STOP and SOCKET_TIMESTAMP data contains
a uint64 value for the time stamp from the current image of the camera (in ns).

The time stamp of the first frame is transmitted as SOCKET_START

The time stamp of the last frame is transmitted as SOCKET_STOP

Any other frames timeStamp is transmitted as SOCKET_TIMESTAMP

The program also transmits the filename on initial trigger in a packet 
of type SOCKET_FILENAME. Note that the filename only contains
Latin-1 characters. This facilitates communication with Matlab.
Avoid any other characters when manually giving a name.


### Socket example in Matlab
- Setting up the socket: `mySocket = tcpclient(127.0.0.1, 20001);`

- Reading the "header": `msgSize = read(mySocket, 1, 'int32');`

`msgType = read(mySocket, 1, 'int32');`

- Reading a timestamp: `timeStamp = read(mySocket, 1, 'uint64');`

- Reading a filename:
`str = char(read(mySocket, msgSize-8));`

`fullFileName = str(5:end); %This is how the String is packed by QT`

# Bugs
Please note that there was a bug in the Pylon SDK. In case PylonRecorder crashed
after 10 Minutes when not triggered, please update to Pylon 5.0.5 libraries.

From the 5.0.5 release Note:
USB3: Fixed an issue where a timeout occurred when the waiting time for an image or a camera event exceeded 10 minutes.

Please report all bugs [here](mailto:sciclist@brain.mpg.de)

# Implementation
The Basler SDK only provides bursts of int64_t number of frames. In practice
this value is (for unknown reasons - 
[please let me know if you have an idea!](mailto:friedrich.kretschmer@brain.mpg.de) )
limited to a maximum of 255, which is not enough for most applications.
To circumvent this limitation the burst mode is started but the `TriggerMode`
property of the camera is immediately set to `Off`, meaning that the camera
is now really running in continuous mode. The number of frames is counted
in the camera event handler to set the `TriggerMode` back to `On` once the
number of frames have been acquired. This results in an extra trigger signal
at the beginning of the recording due to the mode switch.
