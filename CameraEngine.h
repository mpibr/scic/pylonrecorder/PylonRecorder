#ifndef CAMERAENGINE_H
#define CAMERAENGINE_H
#include <QStateMachine>
#include <QHistoryState>
#include <QObject>

class CameraEngine : public QObject
{
    Q_OBJECT
public:
    explicit CameraEngine(QObject *parent = nullptr);

private:
    QStateMachine *stateMachine; /* state machine */

signals:
    void startRecording();
    void startLive();
    void recordingDone();
    void stop();

public slots:
};

#endif // CAMERAENGINE_H
