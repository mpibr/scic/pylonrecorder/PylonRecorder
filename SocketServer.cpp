#include "SocketServer.h"

SocketServer::SocketServer(QObject* parent)
    : QObject(parent)
{
    connect(&server, SIGNAL(newConnection()),
            this, SLOT(onClientConnected()));
    outBuffer.open(QIODevice::ReadWrite);
    outStream.setDevice(&outBuffer);
    outStream.setByteOrder(QDataStream::LittleEndian);
    outStream.setVersion(QDataStream::Qt_5_2);

    inBuffer.open(QIODevice::ReadWrite);
    inStream.setDevice(&inBuffer);
    inStream.setByteOrder(QDataStream::LittleEndian);
    inStream.setVersion(QDataStream::Qt_5_2);
    server.listen(QHostAddress::Any, 20001);
}

SocketServer::~SocketServer()
{
    server.close();
}

void SocketServer::onClientConnected()
{
    client = server.nextPendingConnection();

    connect(client, SIGNAL(readyRead()),
            this, SLOT(onRead()));
    connect(client, SIGNAL(disconnected()),
            this, SLOT(onClientDisconnected()));
    isConnected = true;
    qDebug()<<"Client connected";
    emit(statusInfo("Client connected"));
}

void SocketServer::onClientDisconnected()
{
    isConnected = false;
    qDebug()<<"Client disconnected";
    emit(statusInfo("Client disconnected"));
}

void SocketServer::onRead()
{
    quint32 blockSize;
    quint32 blockType;

    // Read the size.
    readMax(client, sizeof(blockSize));
    inBuffer.seek(0);
    inStream >> blockSize;
    readMax(client, sizeof(blockType));
    inBuffer.seek(0);
    inStream >> blockType;

    switch(blockType){
    case SOCKET_START:
        qDebug()<<"SOCKET_START";
        break;
    }

    // Read the rest of the data.
    //readMax(client, blockSize);
    //buffer.seek(sizeof(blockSize));
}

void SocketServer::onTimeStamp(quint64 timeStamp)
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(16); //Placeholder
        outStream << quint32(SOCKET_TIMESTAMP);
        outStream << timeStamp;
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
    }
}

void SocketServer::onStartTimeStamp(quint64 timeStamp)
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(16); //Placeholder
        outStream << quint32(SOCKET_START);
        outStream << timeStamp;
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
        qDebug()<<"Socket:Start";
    }
}

void SocketServer::onStopTimeStamp(quint64 timeStamp)
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(16); //Placeholder
        outStream << quint32(SOCKET_STOP);
        outStream << timeStamp;
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
        qDebug()<<"Socket:Stop";
    }
}

void SocketServer::onFileName(QString fileName)
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(0); //Placeholder
        outStream << quint32(SOCKET_FILENAME);
        outStream << fileName.toLatin1();
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
        qDebug()<<"Socket:Filename";
    }
}

void SocketServer::onStop()
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(16); //Placeholder
        outStream << quint32(SOCKET_STOP);
        outStream << quint64(0);
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
        qDebug()<<"Socket: Manual stop";
    }
}

void SocketServer::onTrackingResult(std::vector<double> result)
{
    if(isConnected){
        outBuffer.seek(0);
        outStream << quint32(0); //Placeholder
        outStream << quint32(SOCKET_TRACKINGRESULT);
        for(double r : result)
            outStream << r; //Serialize vector
        outBuffer.seek(0);
        outStream << quint32(outBuffer.size()); //Write actual size
        client->write(outBuffer.buffer());
        outBuffer.buffer().clear();
        qDebug()<<"Socket:Tracking Result";
    }
}

void SocketServer::readMax(QIODevice *io, int n)
{
    while(inBuffer.size() < n){
        if(!io->bytesAvailable()){
            io->waitForReadyRead(30000);
        }
        inBuffer.write(io->read(n - inBuffer.size()));
    }
}
