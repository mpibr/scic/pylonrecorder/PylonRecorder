// Deals with (optional) socket communication with clients (e.g. Matlab)

#ifndef SOCKETSocketServer_H
#define SOCKETSocketServer_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QBuffer>
#include <QDataStream>

// Parameters used as Type in outgoing packets
#define SOCKET_TIMESTAMP 0
#define SOCKET_START 1
#define SOCKET_STOP 2
#define SOCKET_FILENAME 3
#define SOCKET_TRACKINGRESULT 4

class SocketServer: public QObject
{
    Q_OBJECT
public:
    explicit SocketServer(QObject * parent = 0);
    ~SocketServer();
public slots:
    void onClientConnected();
    void onClientDisconnected();
    void onRead(); // Preliminary: read data from client
    void readMax(QIODevice *io, int n); // Helper method to read from client

    // Sending of PylonRecorder specific signals
    void onTimeStamp(quint64 timeStamp);               // Send a socket timestamp packet
    void onStartTimeStamp(quint64 timeStamp);          // Send a socket start packet
    void onStopTimeStamp(quint64 timeStamp);           // Send a socket stop packet
    void onFileName(QString fileName);                 // Send a socket filename packet
    void onStop();                                     // Send an socket stop packet without timestamp
    void onTrackingResult(std::vector<double> result); // Send the tracking result
private:
    bool isConnected = false;
    QTcpServer server;
    QTcpSocket* client;
    QBuffer outBuffer;
    QBuffer inBuffer;
    QDataStream outStream; // To serialize outgoing data
    QDataStream inStream;  // To serialize incoming data

signals:
    void statusInfo(QString info);

};

#endif // SOCKETSocketServer_H
