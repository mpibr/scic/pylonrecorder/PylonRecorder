#include "TrackerRecordingWorker.h"

TrackerRecordingWorker::TrackerRecordingWorker(QObject* parent) :
    QObject(parent)
{
}

void TrackerRecordingWorker::onTrackingResult(std::vector<double> data){
    if(logFile.isOpen()){
        QTextStream out(&logFile);
        for(const auto &d : data){
            out << QString::number(d) << "\t";
        }
        out << "\n";
    }
}

void TrackerRecordingWorker::onTrackingMessage(std::string message)
{
    if(logFile.isOpen()){
        QTextStream out(&logFile);
            out << QString::fromStdString(message);
        out << "\n";
    }
}

void TrackerRecordingWorker::onTextFileOpen(QString fileName){
    if(logFile.isOpen())
        logFile.close();
    logFile.setFileName(fileName);
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);
    qDebug() << "TrackerRecordingWorker opened: " << fileName;
}

void TrackerRecordingWorker::onTextFileClose(){
    logFile.close();
}
