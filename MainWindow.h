#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QLabel>
#include <QThread>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QDateTime>
#include <QFileInfo>
#include <QDesktopWidget>
#include <QPluginLoader>
#include <opencv2/core/core.hpp>
#include <pylon/PylonIncludes.h>
#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include "RecordingWorker.h"
#include "TrackerRecordingWorker.h"
#include "SocketServer.h"
#include "TimeConversion.h"
#include "TrackerInterface.h"
#include "Capture.h"
#include "CommandLineOptions.h"
#include "RamMonitor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

public:
    explicit MainWindow(Capture* capture, CommandLineOptions options, QWidget *parent = 0);
    ~MainWindow();
    QImage putImage(const cv::Mat& mat);

signals:
    // To Recorder
    void videoOpen(QString fileName, int frameRate, cv::Size frameSize);
    void videoClose();
    void textFileOpen(QString fileName);
    void textFileClose();
    void setNImagesToRecord(long long);
    void openedFileName(QString fileName);

private slots:
    // Automatic connections
    void on_lineEdit_nFrames_editingFinished();
    void on_pushButton_softwareTrigger_clicked();
    void on_checkBox_record_clicked(bool checked);
    void on_pushButton_selectFile_clicked();
    void on_pushButton_stop_clicked();
    void on_checkBox_externalTrigger_clicked(bool checked);
    void on_radioButton_continuous_clicked();
    void on_radioButton_burst_clicked();
    void on_checkBox_autoFileName_clicked(bool checked);
    void on_lineEdit_fileName_editingFinished();
    void on_checkBox_trackingPreview_clicked(bool checked);
    void on_checkBox_recordTracking_clicked(bool checked);
    void on_radioButton_frameWise_clicked();

    // From Capture
    void onBurstStarted(quint64 timeStamp);
    void onBurstStopped(quint64 timeStamp);
    void onFrameGrabbed(cv::Mat);
    void onUserInput(bool value);

    // From Recorder
    void onFrameWritten(long long);
    void onAllWritten();

    // From Ram Monitor
    void onAvailableRam(long availableRam);

    void onStatusInfo(QString info);
    void onAbout();

    void on_pushButton_selectTrackingFile_clicked();

    void on_lineEdit_trackingFileName_editingFinished();

private:
    Ui::MainWindow *ui;
    QLabel *label_recorded;
    QLabel *label_acquired;
    QLabel *label_statusSeparator;
    QLabel *label_availableRAM;
    QLabel *label_totalRAM;
    QLabel *label_RAMSeparator;

    QString fileName;
    QString filePath = ".";
    QString lineEditFileName = "tmp.avi";
    QString autoFileName = "tmp.avi";
    QString autoFileNameTemplate = "yyyy-MM-dd-hh-mm-ss.avi";

    QString trackingFileName;
    QString trackingFilePath = ".";
    QString lineEditTrackingFileName = "tmp.csv";
    QString autoTrackingFileName = "tmp.csv";
    QString autoTrackingFileNameTemplate = "yyyy-MM-dd-hh-mm-ss.csv";

    QString cameraIdxString; // for naming of files only

    QString cameraInfo;
    bool isRecording = false;
    bool isTrackingRecording = false;
    bool wasWritten = true;
    bool isStopped = false;
    int displayItv = 0;
    int captureMode = CAPTURE_CONTINUOUS;
    long long nImagesToGrab = 0;
    int grabbed = 0;

    RamMonitor* ramMonitor = nullptr;
    Capture *capture = nullptr;
    SocketServer *socketServer = nullptr;
    RecordingWorker *recordingWorker = nullptr;
    TrackerRecordingWorker *trackerRecordingWorker = nullptr;
    QThread *recordingThread = nullptr;
    QThread *trackingThread = nullptr;
    QThread *ramMonitorThread = nullptr;
    TrackerInterface *trackerInterface = nullptr;

    QGraphicsScene* scene;
    QGraphicsPixmapItem* pixmapItem;

    void resizeEvent(QResizeEvent* event);
    void showEvent(QShowEvent * event);
};

#endif // MAINWINDOW_H
