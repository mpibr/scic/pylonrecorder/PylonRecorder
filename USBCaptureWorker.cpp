#include "USBCaptureWorker.h"

USBCaptureWorker::USBCaptureWorker(cv::VideoCapture* cap)
{
    this->cap = cap;
}

void USBCaptureWorker::onCaptureNImages(long long nImagesToGrab)
{
    long long nImagesGrabbed = 0;
    running = true;
    emit(startTimeStampEvent(qTime.elapsed()));
    while(running){
        cv::Mat frame;
        *cap >> frame;

        nImagesGrabbed++;

        // The event of the last frame should be received before the last frame
        if(((nImagesToGrab>0) & (nImagesGrabbed == nImagesToGrab)) | (nImagesGrabbed == std::numeric_limits <long long>::max())){
            running = false;
            emit(stopTimeStampEvent(qTime.elapsed()));
        }

        emit(sendFrame(frame.clone()));
        QApplication::processEvents();
        QThread::sleep(1/frameRate);
    }
}

void USBCaptureWorker::onSetFrameRate(int frameRate)
{
    this->frameRate = frameRate;
}

void USBCaptureWorker::onStopGrabbing()
{
    running = false;
}
