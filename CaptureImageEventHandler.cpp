#include "CaptureImageEventHandler.h"

CaptureImageEventHandler::CaptureImageEventHandler(QObject* parent)
    : QObject(parent)
{
    // Specify the output pixel format.
    formatConverter.OutputPixelFormat = Pylon::PixelType_BGR8packed;
}

void CaptureImageEventHandler::OnImageGrabbed( Pylon::CInstantCamera& camera, const Pylon::CGrabResultPtr& ptrGrabResult)
{
    //Line 3 was set High. Emit lineStopEvent()
    if(!stopLineString.empty()){
        GenApi::CEnumerationPtr(camera.GetNodeMap().GetNode("LineSelector"))->FromString(stopLineString);
        bool isHigh = GenApi::CBooleanPtr(camera.GetNodeMap().GetNode("LineStatus"))->GetValue();

        if(!lineWasHigh & isHigh){
            emit(lineInputEvent(true)); //line toggled from low to high
            qDebug()<<"CaptureImageEventHandler: Line3 turned high";
            lineWasHigh = true;
        }else if(lineWasHigh & !isHigh){
            emit(lineInputEvent(false)); // line toggled from high to low
            qDebug()<<"CaptureImageEventHandler: Line3 turned low";
            lineWasHigh = false;
        }
    }

    // Image grabbed successfully?
    if (ptrGrabResult->GrabSucceeded()){

        quint64 timeStamp = ptrGrabResult->GetTimeStamp();
        formatConverter.Convert(pylonImage, ptrGrabResult);

        // Create an OpenCV image from a pylon image.
        cv::Mat cvMat = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *) pylonImage.GetBuffer());

        switch(eventStatus){
        case EVENT_START:
            emit(startTimeStampEvent(timeStamp));
            eventStatus = EVENT_TIMESTAMP;
            break;
        case EVENT_STOP:
            emit(stopTimeStampEvent(timeStamp));
            eventStatus = EVENT_TIMESTAMP;
            break;
        case EVENT_TIMESTAMP:
            emit(timeStampEvent(timeStamp));
            break;
        }

        emit(imageEvent(cvMat.clone()));

    }else{
        std::cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << std::endl;
    }
}

void CaptureImageEventHandler::onBurstStartEvent()
{
    eventStatus = EVENT_START;
}

void CaptureImageEventHandler::onBurstStopEvent()
{
    eventStatus = EVENT_STOP;
}
