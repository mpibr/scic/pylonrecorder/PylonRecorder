// Definition of the general interface for the worker thread of a plugin

#ifndef TRACKERWORKERINTERFACE_H
#define TRACKERWORKERINTERFACE_H

#include <opencv2/core/core.hpp>
class TrackerWorkerInterface
{

public:
    // Slot that receives the actual frame
    void onFrameGrabbed(cv::Mat);

    // Emits the original image overlayed with tracking location etc. (Used as a signal)
    void trackingPreview(cv::Mat);

    // Emits a user defined signal (can be used to output something on GPIO pins of camera
    void userOutput(bool value);

    // Receives a user defined signal (e.g. from GPIO pins of camera)
    void onUserInput(bool value);

    // Emits the result of tracking (Used as a signal)
    void trackingResult(std::vector<double> data);

    // Emits a general message (e.g. for additional information in logfile, used as a signal)
    void trackingMessage(std::string message);

};

#endif // TRACKERWORKERINTERFACE_H
