#ifndef COMMANDLINEOPTIONS_H
#define COMMANDLINEOPTIONS_H
#include <QString>

struct CommandLineOptions{
    uint wndX;
    uint wndY;
    uint wndW;
    uint wndH;
    uint displayFrameRate;
    QString defaultPath;
    int cameraIdx;
    QString configFile;
    int recordingCodec;
    float compression;
    bool autoFileName;
    bool record;
    uint stopLine;
    bool external;
    int captureMode;
    uint framesToCapture;
};

#endif // COMMANDLINEOPTIONS_H
