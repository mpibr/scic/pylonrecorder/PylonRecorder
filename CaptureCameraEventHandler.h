//Pylon specific CCameraEventHandler

#ifndef CAPTURECAMERAEVENTHANDLER_H
#define CAPTURECAMERAEVENTHANDLER_H
#include <QWidget>
#include <QString>
#include <QDebug>
#include <pylon/PylonIncludes.h>
#include "CaptureEvents.h"

class CaptureCameraEventHandler :  public QObject, public Pylon::CCameraEventHandler
{
    Q_OBJECT

    enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

public:
    inline explicit CaptureCameraEventHandler(QObject* parent = 0) : QObject(parent){}
    virtual void OnCameraEvent( Pylon::CInstantCamera& camera, intptr_t userProvidedId, GenApi::INode* );

private:
    long long nImagesGrabbed = 0;
    long long nImagesToGrab = 0;
    int captureMode = CAPTURE_CONTINUOUS;

signals:
    void statusInfo(QString info);
    void burstStartEvent();
    void burstStopEvent();

public slots:
    void onCounterReset();
    void onSetImagesToGrab(long long);
    void onSetCaptureMode(int captureMode);

};

#endif // CAPTURECAMERAEVENTHANDLER_H
