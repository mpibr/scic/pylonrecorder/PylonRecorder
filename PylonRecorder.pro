#-------------------------------------------------
#
# Project created by QtCreator 2016-05-19T10:48:47
#
#-------------------------------------------------

QT       += core gui network
RC_FILE = app.rc
RESOURCES = application.qrc
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PylonRecorder
DEFINES += APP_VERSION=\\\"0.28\\\"
DEFINES += USB_OPENCV
TEMPLATE = app

mac{
    QMAKE_CXXFLAGS += -F/Library/Frameworks
    INCLUDEPATH += /usr/local/opt/opencv3/include
    LIBS += -L/usr/local/opt/opencv3/lib
    LIBS += -lopencv_core
    LIBS += -lopencv_highgui
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_video
    LIBS += -lopencv_videoio

    INCLUDEPATH += /Users/kretschmerf/pylonHeaders
    INCLUDEPATH +=  /Users/kretschmerf/pylonHeaders/pylon/GenICam

    LIBS += -F/Library/Frameworks/ -framework pylon
    LIBS += -L/Library/Frameworks/pylon.framework/Versions/A/Libraries
    LIBS += -lpylonbase-5.0.11
    LIBS += -lpylonutility
    LIBS += -lGenApi_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lGCBase_gcc_v3_0_Basler_pylon_v5_0

    INCLUDEPATH += /usr/local/Cellar/qwt/6.1.3_1/lib/qwt.framework/Headers
}

unix{
    CONFIG += link_pkgconfig
    PKGCONFIG += opencv

    #INCLUDEPATH += /usr/include/qwt
    INCLUDEPATH += /opt/pylon5/include
#    INCLUDEPATH += /usr/local/include/opencv2
#    LIBS += -L/usr/local/lib
#    LIBS += -lopencv_core
#    LIBS += -lopencv_highgui
#    LIBS += -lopencv_imgproc
#    LIBS += -lopencv_features2d
#    LIBS += -lopencv_videoio
#    LIBS += -lopencv_video
#    LIBS += -lopencv_videostab

    LIBS += -L/opt/pylon5/lib64
    LIBS += -lpylonbase-5.0.11
    #LIBS += -lpylonbase-5.0.5
    LIBS += -lpylonutility
    LIBS += -lGenApi_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lLog_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lMathParser_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lNodeMapData_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lXmlParser_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lGCBase_gcc_v3_0_Basler_pylon_v5_0
    QMAKE_CXXFLAGS += -Wno-unknown-pragmas
}

win32 {
 DEFINES += NOMINMAX
    INCLUDEPATH += "D:\\opencv\\build_vc12\\install\\include"

    CONFIG(debug,debug|release) {
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }
    INCLUDEPATH += "C:\\Qwt-6.1.3\\include"
    LIBS += -L"C:\\Qwt-6.1.3\\lib" \
        -lqwt

#For this version of Visual C++  Use this compiler version
#Visual C++ 4.x                  MSC_VER=1000
#Visual C++ 5                    MSC_VER=1100
#Visual C++ 6                    MSC_VER=1200
#Visual C++ .NET                 MSC_VER=1300
#Visual C++ .NET 2003            MSC_VER=1310
#Visual C++ 2005  (8.0)          MSC_VER=1400
#Visual C++ 2008  (9.0)          MSC_VER=1500
#Visual C++ 2010 (10.0)          MSC_VER=1600
#Visual C++ 2012 (11.0)          MSC_VER=1700
#Visual C++ 2013 (12.0)          MSC_VER=1800!!
#Visual C++ 2015 (14.0)          MSC_VER=1900

    INCLUDEPATH += "C:/Program Files/Basler/pylon 5/Development/include"

    LIBS += "-LC:/Program Files/Basler/pylon 5/Development/lib/x64" \
        -lGenApi_MD_VC120_v3_0_Basler_pylon_v5_0 \
        -lGCBase_MD_VC120_v3_0_Basler_pylon_v5_0 \
        -lPylonBase_MD_VC120_v5_0 \
        -lPylonUtility_MD_VC120_v5_0
}

CONFIG += c++11

SOURCES += main.cpp\
        MainWindow.cpp \
    RecordingWorker.cpp \
    SocketServer.cpp \
    AboutDialog.cpp \
    TimeConversion.cpp \
    CaptureCameraEventHandler.cpp \
    CaptureImageEventHandler.cpp \
    PylonCapture.cpp \
    USBCapture.cpp \
    USBCaptureWorker.cpp \
    TrackerRecordingWorker.cpp \
    RamMonitor.cpp \
    CameraEngine.cpp

HEADERS  += MainWindow.h \
    RecordingWorker.h \
    SocketServer.h \
    AboutDialog.h \
    TimeConversion.h \
    CAcquireBurstSoftwareTriggerConfiguration.h \
    CaptureEvents.h \
    CaptureImageEventHandler.h \
    CaptureCameraEventHandler.h \
    PylonCapture.h \
    Capture.h \
    USBCapture.h \
    USBCaptureWorker.h \
    CommandLineOptions.h \
    TrackerInterface.h \
    TrackerWorkerInterface.h \
    TrackerRecordingWorker.h \
    RamMonitor.h \
    CameraEngine.h

FORMS    += MainWindow.ui \
    AboutDialog.ui

DISTFILES += \
    PylonRecorderModel.qmodel \
    app.rc

RESOURCES +=

OTHER_FILES += \
    README.md \
    mpi-brain-research.png \
    PylonRecorder.ico \
    app.rc \
    logo_scic.png


