#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include <QDebug>
AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{

    ui->setupUi(this);
    ui->label_title->setText(QApplication::applicationName() + " " + QString(APP_VERSION));
    ui->label_mpiLogo->setPixmap(QPixmap(":/logos/mpi-brain-research.png"));
    ui->label_scicLogo->setPixmap(QPixmap(":/logos/logo_scic.png"));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
