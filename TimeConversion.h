#ifndef TIMECONVERSION_H
#define TIMECONVERSION_H
#include <QString>

class TimeConversion
{
public:
    // Converts time in ms into string of form [hh:mm:ss:ms]
    static QString stringConvert(int msec);
};

#endif // TIMECONVERSION_H
