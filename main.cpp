#include "MainWindow.h"
#include <QApplication>
#include <QCommandLineParser>
#include <CommandLineOptions.h>
#include "Capture.h"
#include "PylonCapture.h"
#include "USBCapture.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("PylonRecorder");
    QCoreApplication::setApplicationVersion(APP_VERSION);
    a.setStyleSheet("QStatusBar::item { border: none }");

    QCommandLineParser parser;

    // An option with a value
    QCommandLineOption wndXOption(QStringList() << "wndx" << "window-x",
                                  QCoreApplication::translate("main", "Open the programs window at x <pixel>"),
                                  QCoreApplication::translate("main", "pixel"), "10");

    QCommandLineOption wndYOption(QStringList() << "wndy" << "window-y",
                                  QCoreApplication::translate("main", "Open the programs window at y <pixel>"),
                                  QCoreApplication::translate("main", "pixel"), "30");

    QCommandLineOption wndWOption(QStringList() << "wndw" << "window-width",
                                  QCoreApplication::translate("main", "Set the programs window width to <pixel>"),
                                  QCoreApplication::translate("main", "pixel"), "1200");

    QCommandLineOption wndHOption(QStringList() << "wndh" << "window-height",
                                  QCoreApplication::translate("main", "Set the programs window height <pixel>"),
                                  QCoreApplication::translate("main", "pixel"), "750");

    QCommandLineOption defaultPathOption(QStringList() << "p" << "path",
                                         QCoreApplication::translate("main", "Set the programs path to <directory>"),
                                         QCoreApplication::translate("main", "directory"), ".");

    QCommandLineOption displayFrameRateOption(QStringList() << "df" << "display-framerate",
                                              QCoreApplication::translate("main", "Set the programs display framerate <framerate>"),
                                              QCoreApplication::translate("main", "framerate"), "10");

    QCommandLineOption cameraIdxOption(QStringList() << "cam" << "camera-idx",
                                       QCoreApplication::translate("main", "Try to use camera <idx>"),
                                       QCoreApplication::translate("main", "idx"), "-1");

    QCommandLineOption configFileOption(QStringList() << "cfg" << "config-file",
                                        QCoreApplication::translate("main", "Load or save configuration from/to <file> (load if <file> exists)"),
                                        QCoreApplication::translate("main", "file"), "");

    QCommandLineOption codecOption(QStringList() << "codec" << "recording-codec",
                                   QCoreApplication::translate("main", "Record using codec <codec> (e.g. h264 (default), mjpeg or none)"),
                                   QCoreApplication::translate("main", "codec"), "h264");

    QCommandLineOption compressionOption(QStringList() << "compression" << "compression-quality",
                                   QCoreApplication::translate("main", "Set compression quality of codec to <value> (0-100, default = -1 = codec specific)"),
                                   QCoreApplication::translate("main", "compression"), "-1");

    QCommandLineOption autoFileNameOption("autofilename",
                                         QCoreApplication::translate("main", "Enable automated naming of files on startup"));

    QCommandLineOption stopLineOption("stopline",
                                  QCoreApplication::translate("main", "Enable hardware recoding stop on <line>"),
                                  QCoreApplication::translate("main", "line"), "0");

    QCommandLineOption recordOption("record",
                                         QCoreApplication::translate("main", "Enable recording on startup"));

    QCommandLineOption externalOption("external",
                                         QCoreApplication::translate("main", "Enable external triggering on startup"));

    QCommandLineOption continuousCaptureModeOption("continuous",
                                         QCoreApplication::translate("main", "Start camera in continuous mode"));

    QCommandLineOption framewiseCaptureModeOption("framewise",
                                         QCoreApplication::translate("main", "Start camera in framewise mode"));

    QCommandLineOption burstCaptureModeOption("burst",
                                         QCoreApplication::translate("main", "Start camera in burst mode"));

    QCommandLineOption framesToCapture(QStringList() << "frames",
                                   QCoreApplication::translate("main", "Record number of <frames>"),
                                   QCoreApplication::translate("main", "frames"), "0");

    parser.addOption(wndXOption);
    parser.addOption(wndYOption);
    parser.addOption(wndWOption);
    parser.addOption(wndHOption);
    parser.addOption(defaultPathOption);
    parser.addOption(displayFrameRateOption);
    parser.addOption(cameraIdxOption);
    parser.addOption(configFileOption);
    parser.addOption(codecOption);
    parser.addOption(compressionOption);
    parser.addOption(autoFileNameOption);
    parser.addOption(stopLineOption);
    parser.addOption(recordOption);
    parser.addOption(externalOption);
    parser.addOption(continuousCaptureModeOption);
    parser.addOption(framewiseCaptureModeOption);
    parser.addOption(burstCaptureModeOption);
    parser.addOption(framesToCapture);

    //parser.addPositionalArgument("camera", QCoreApplication::translate("main", "camera index to open, optionally."));
    parser.setApplicationDescription("help");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(a);

    CommandLineOptions options;
    options.wndX = parser.value(wndXOption).toInt();
    options.wndY = parser.value(wndYOption).toInt();
    options.wndW = parser.value(wndWOption).toInt();
    options.wndH = parser.value(wndHOption).toInt();
    options.displayFrameRate = parser.value(displayFrameRateOption).toInt();
    options.defaultPath = parser.value(defaultPathOption);
    options.cameraIdx = parser.value(cameraIdxOption).toInt();
    options.configFile = parser.value(configFileOption);
    QString codecString = parser.value(codecOption);

    if(codecString.compare("none")==0){
        options.recordingCodec = 0;
    }else if(codecString.compare("mjpeg")==0){
        options.recordingCodec = CV_FOURCC('M','J','P','G');
    }else{
        options.recordingCodec = CV_FOURCC('X','2','6','4');
    }

    options.stopLine = parser.value(stopLineOption).toInt();
    options.compression = parser.value(compressionOption).toFloat();
    options.autoFileName = parser.isSet(autoFileNameOption);
    options.record = parser.isSet(recordOption);
    options.external = parser.isSet(externalOption);
    options.framesToCapture = parser.value(framesToCapture).toInt();

    //if (parser.isSet(continuousCaptureModeOption)){
    options.captureMode = CAPTURE_CONTINUOUS;
    if (parser.isSet(framewiseCaptureModeOption))
            options.captureMode = CAPTURE_FRAMEWISE;
    if (parser.isSet(burstCaptureModeOption))
            options.captureMode = CAPTURE_BURST;

    Capture* capture = new PylonCapture(0, options.cameraIdx, options.configFile);
#ifdef USB_OPENCV
    if(!capture->isAvailable){
        delete(capture);
        capture = new USBCapture(0, options.cameraIdx, options.configFile);
    }
#endif
    // Quit if no camera was found
    if(!capture->isAvailable){
        delete(capture);
        QMessageBox *msgBox = new QMessageBox();
        msgBox->setText("No compatible cameras detected");
        msgBox->setAttribute(Qt::WA_DeleteOnClose, true);
        msgBox->exec();
        a.exit();
    }else{
        MainWindow w(capture, options);
        w.show();
        return a.exec();
        delete(capture);
    }
}
