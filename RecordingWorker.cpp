#include "RecordingWorker.h"

RecordingWorker::RecordingWorker(QObject* parent)
    : QObject(parent)
{
}

void RecordingWorker::onVideoOpen(QString fileName, int frameRate, cv::Size frameSize)
{
    if(!fileName.isEmpty()){

    QFileInfo fileInfo(fileName);

        QStorageInfo storage(fileInfo.absolutePath());
        if (storage.isReadOnly()){
            QMessageBox::warning(0,tr("Warning"),
                                         QString("Storage location is read only!"));
            return;
        }

        qDebug() << "name:" << storage.name();
        qDebug() << "fileSystemType:" << storage.fileSystemType();
        qDebug() << "size:" << storage.bytesTotal()/1000/1000 << "MB";
        qDebug() << "availableSize:" << storage.bytesAvailable()/1000/1000 << "MB";

        if(storage.bytesAvailable()/1000/1000 < DiskFullWarningLimit){
            QMessageBox::warning(0,tr("Warning"),
                                         QString("Less than ").append(QString::number(DiskFullWarningLimit).append("MB available for storage!")));
            return;
        }

        if(videoWriter.isOpened()){
            videoWriter.release();
        }

// This is a potential implementation for using gstreamer instead of ffmpeg in future
//        int codec = CV_FOURCC('H','E','V','1');
//        int codec = CV_FOURCC('H','V','C','1');
//        QString gStreamerPipeline = "appsrc ! autovideoconvert ! x264enc ! matroskamux ! filesink location=";
//        gStreamerPipeline.append(fileName).append(" ");
//        videoWriter.open(gStreamerPipeline.toStdString(), 0, frameRate, frameSize, true); //gstreamer pipeline

        if(compression > -1){
            videoWriter.set(cv::VIDEOWRITER_PROP_QUALITY, compression);
        }

        //videoWriter.open(fileName.toStdString(), cv::CAP_FFMPEG, codec, frameRate, frameSize, true);
        videoWriter.open(fileName.toStdString(), codec, frameRate, frameSize, true);
        if(!videoWriter.isOpened()){
            emit(statusInfo("Recorder: Unable to open file "));
            qDebug()<<"Recorder: Unable to open file "<<fileName;
        }else{
            emit(statusInfo("Recorder: Opened " + fileName));
            qDebug() << "Recorder: Opened " <<fileName;
            nImagesGrabbed = 0;
        }
    }

}


void RecordingWorker::onFrameGrabbed(cv::Mat im)
{
    if(videoWriter.isOpened()){
        videoWriter.write(im);
        nImagesGrabbed++;
        emit(frameWritten(nImagesGrabbed));
        //qDebug() << "Recorder: " << nImagesGrabbed;
    }
}

// Stay here until the video was fully written or closed
void RecordingWorker::onVideoClose(){
    if(videoWriter.isOpened()){
        emit(statusInfo("Recorder: Waiting for all frames"));
        qDebug()<<"Recorder: Waiting for all frames";

        // Process remaining events until event queue is empty
        while(videoWriter.isOpened()){
            if((nImagesToGrab>0) & (nImagesGrabbed>=nImagesToGrab)){
                videoWriter.release();
            }
            QApplication::processEvents();
        }
        emit(statusInfo("Recorder: all frames written to disk"));
        qDebug() << "Recorder: Got all frames!";
    }

    qDebug()<<"Recorder: all frames written";
    emit(allWritten());
}

void RecordingWorker::onSetNImagesToGrab(long long nImagesToGrab){
    this->nImagesToGrab = nImagesToGrab;
    qDebug() << "Recorder: Number of frames to record was changed to " << nImagesToGrab;
}

