#include "CameraEngine.h"

CameraEngine::CameraEngine(QObject *parent) : QObject(parent)
{
    stateMachine = new QStateMachine(this);
    QState *recordingState = new QState(stateMachine);
    QState *writingState = new QState(stateMachine);
    QState *idleState = new QState(stateMachine);
    QState *liveState = new QState(stateMachine);

    QHistoryState* lastState = new QHistoryState(stateMachine);

    idleState->addTransition(this, SIGNAL(startRecording()), recordingState);
    recordingState->addTransition(this, SIGNAL(stop()), writingState);
    writingState->addTransition(this, SIGNAL(recordingDone()), idleState);

    idleState->addTransition(this, SIGNAL(startLive()), liveState);
    idleState->addTransition(this, SIGNAL(stop()), idleState);

}
