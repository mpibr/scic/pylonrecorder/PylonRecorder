#include "PylonCapture.h"

PylonCapture::PylonCapture(QObject *parent, int cameraIdx, QString configFile)
    : Capture(parent)
{
    try{
        // Get the transport layer factory.
        Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();

        Pylon::DeviceInfoList_t devices;
        tlFactory.EnumerateDevices(devices, false);
        qDebug()<< "PylonRecorder found: " << devices.size() << " cameras!";
        if(devices.size()==0){
            throw RUNTIME_EXCEPTION("No compatible Basler cameras found");
        }

        if(cameraIdx >= 0){
            if((uint)cameraIdx < devices.size()){
                if(tlFactory.IsDeviceAccessible(devices[cameraIdx])){
                    pDevice = tlFactory.CreateDevice(devices[cameraIdx]);
                }
            }else{
                throw RUNTIME_EXCEPTION("Camera index > detected cameras");
            }

        }else{
            for(cameraIdx = 0; (uint)cameraIdx < devices.size(); cameraIdx++){
                //Try to access first available camera
                if(tlFactory.IsDeviceAccessible(devices[cameraIdx])){
                    pDevice = tlFactory.CreateDevice(devices[cameraIdx]);
                    break;
                }
            }
        }

        camera = new Pylon::CInstantCamera();
        camera->Attach(pDevice);

        // Print the model name of the camera.
        modelName = QString(camera->GetDeviceInfo().GetModelName());

        // When using the grab loop thread provided by the Instant Camera object, an image event handler processing the grab
        // results must be created and registered.
        cImageEventGrabber = new CaptureImageEventHandler(this);
        cCameraEventHandler = new CaptureCameraEventHandler(this);

        if (stopLine != 0){
            stopLineString = GenICam::gcstring(QString("Line").append(QString::number(stopLine)).toLatin1());
            cImageEventGrabber->stopLineString = stopLineString;
        }

        if (triggerLine != 0)
            triggerLineString = GenICam::gcstring(QString("Line").append(QString::number(triggerLine)).toLatin1());

        connect(cImageEventGrabber, SIGNAL(imageEvent(cv::Mat)), this, SIGNAL(sendFrame(cv::Mat)));
        connect(cImageEventGrabber, SIGNAL(startTimeStampEvent(quint64)), this, SIGNAL(startTimeStampEvent(quint64)));
        connect(cImageEventGrabber, SIGNAL(stopTimeStampEvent(quint64)), this, SIGNAL(stopTimeStampEvent(quint64)));
        connect(cImageEventGrabber, SIGNAL(timeStampEvent(quint64)), this, SIGNAL(timeStampEvent(quint64)));
        connect(cImageEventGrabber, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));
        connect(cImageEventGrabber, SIGNAL(lineInputEvent(bool)), this, SIGNAL(lineInputEvent(bool)));

        connect(cCameraEventHandler, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));

        // These events are forwarded to add a timeStamp
        connect(cCameraEventHandler, SIGNAL(burstStartEvent()), cImageEventGrabber, SLOT(onBurstStartEvent()));
        connect(cCameraEventHandler, SIGNAL(burstStopEvent()), cImageEventGrabber, SLOT(onBurstStopEvent()));

        connect(this, SIGNAL(setImagesToCapture(long long)), cCameraEventHandler, SLOT(onSetImagesToGrab(long long)));
        connect(this, SIGNAL(setCaptureMode(int)), cCameraEventHandler, SLOT(onSetCaptureMode(int)));
        connect(this, SIGNAL(resetCounter()), cCameraEventHandler, SLOT(onCounterReset()));

        camera->RegisterImageEventHandler( cImageEventGrabber, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);

        // Register events

        // FrameStart is not available on some cameras. Try to use ExposireEnd instead
        Pylon::String_t frameEventName("FrameStart");
        if(!GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName(frameEventName))){
            frameEventName = "ExposureEnd";
        }

        camera->RegisterCameraEventHandler( cCameraEventHandler, Pylon::String_t("Event").append(frameEventName), captureFrameStartEvent, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_None);
        camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);
        camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameBurstStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);

        // Register the standard configuration event handler for enabling software triggering.
        // The software trigger configuration handler replaces the default configuration
        // as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
        camera->RegisterConfiguration( new Pylon::CAcquireBurstSoftwareTriggerConfiguration, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);

        // More configurations:
        //camera->RegisterConfiguration( new CAcquireContinuousConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        //camera->RegisterConfiguration( new CAcquireSingleFrameConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        //camera->RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);

        // Camera event processing must be activated first, the default is off.
        camera->GrabCameraEvents = true;


        camera->Open();
        if(!configFile.isEmpty()){
            QFileInfo pfsFileInfo(configFile);

            if(pfsFileInfo.exists()){
                Pylon::CFeaturePersistence::Load( configFile.toLatin1().data(), &camera->GetNodeMap(), true );
                emit(statusInfo("Loaded configuration from " + configFile));
            }else{
                Pylon::CFeaturePersistence::Save( configFile.toLatin1().data(), &camera->GetNodeMap() );
            }
        }

        // Set Camera parameters
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString(frameEventName);
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        //GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStart");
        //GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameStartOvertrigger");
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStartOvertrigger");
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");

        // Use fixed FrameRate
        //GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->SetValue(true);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Width"))->SetValue(width);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Height"))->SetValue(width);
        //GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(frameRate);

        // Get Camera parameters
        GenApi::CIntegerPtr width = camera->GetNodeMap().GetNode("Width");
        GenApi::CIntegerPtr height = camera->GetNodeMap().GetNode("Height");
        frameSize = cv::Size((int)width->GetValue(), (int)height->GetValue());
        frameRate = GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->GetValue();

        // Low level usb tweaks
        //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxNumBuffer"))->SetValue(8); //16
        //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxBufferSize"))->SetValue(13107200); //131072
        //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxTransferSize"))->SetValue(32768); //65536
        //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("NumMaxQueuedUrbs"))->SetValue(64); //64
        //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("TransferLoopThreadPriority"))->SetValue(5);



        // For each output line, a specific UserOutput parameter is available to set the line as "user settable":
        // UserOutput 1 is available for output line Line 2
        //
        // UserOutput 2 is available for GPIO line Line 3 if the line is configured for output
        //
        // UserOutput 3 is available for GPIO line Line 4 if the line is configured for output.
        //
        // Set output line Line 2 to user settable

        qDebug()<< "PylonCapture: Testing LineModes to configure user output.";

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line3"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line3"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput2"))){
                    //Line 3 is set for output
                    qDebug() << "PylonCapture: User defined output on Line3 (UserOutput2)";
                    outputString = "UserOutput2";
                }
            }else{
                if (stopLine != 3) //If this Line is not used as stopLine, use as trigger
                    triggerLine = 3;
                qDebug()<<"PylonCapture: External trigger on Line4";
            }
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line4"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line4"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput3"))){
                    if(outputString.empty()){
                        //Line 4 is set for output
                        qDebug() << "PylonCapture: User defined output on Line4 (UserOutput3)";
                        outputString = "UserOutput3";
                    }
                }
            //If this Line is not used as stopLine & trigger not set, use as trigger
            }else if(stopLine != 4 && triggerLine == 0){
                triggerLine = 4;
            }
        }

        isAvailable = true;
    }
    catch (GenICam::GenericException &e)
    {
        isAvailable = false;
        Pylon::PylonTerminate();
        // Error handling.
        std::cerr << "An exception occurred." << std::endl
                  << e.GetDescription() << std::endl;
    }
}

void PylonCapture::setFrameRate(int frameRate)
{
    GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(frameRate);
}

void PylonCapture::init(int captureMode, long long nImagesToCapture)
{
    // Start the grabbing using the grab loop thread, by setting the grabLoopType parameter
    // to GrabLoop_ProvidedByInstantCamera. The grab results are delivered to the image event handlers.
    // The GrabStrategy_OneByOne default grab strategy is used.
    camera->StopGrabbing();
    setTriggered(true);

    // This is required to to the implementation of the callback
    // one frame more needs to be acquired for completion
    if(nImagesToCapture>0)
        nImagesToCapture++;

    emit(setImagesToCapture(nImagesToCapture));
    emit(setCaptureMode(captureMode));
    emit(resetCounter());
    switch(captureMode){
    case CAPTURE_CONTINUOUS:
        camera->StartGrabbing( Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized continuous grabbing";
        break;
    case CAPTURE_BURST:
        if(nImagesToCapture>0){
            camera->StartGrabbing( nImagesToCapture, Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }else{
            camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }
        qDebug()<<"PylonCapture: Initialized burst grabbing";
        break;
    case CAPTURE_FRAMEWISE:
        camera->StartGrabbing( Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized framwise grabbing";
        break;
    }

    emit(statusInfo("Camera initialized"));
}

void PylonCapture::softwareTrigger()
{
    camera->ExecuteSoftwareTrigger();
}

void PylonCapture::setExternalTrigger(bool hasExternalTrigger)
{
    if(triggerLine != 0){
        if(hasExternalTrigger){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString(triggerLineString);
        }else{
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString("Software");
        }
    }
}

void PylonCapture::setUserOutput(bool value)
{
    if(!outputString.empty()){
        qDebug()<< "PylonCapture: User output " << value;
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("UserOutputSelector"))->FromString(outputString);
        GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("UserOutputValue"))->SetValue(value);
    }
}

void PylonCapture::setTriggered(bool isTriggered)
{
    if(isTriggered)
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "On");
    else
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");
}

PylonCapture::~PylonCapture()
{
    if(isAvailable){
        camera->StopGrabbing();
        camera->Close();
        camera->DeregisterImageEventHandler(cImageEventGrabber);

        camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameStart");
        //camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameBurstStart");
        camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameStartOvertrigger");
        camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameBurstStartOvertrigger");

        if(pDevice->IsOpen()){
            pDevice->Close();
        }
        // Releases all pylon resources.
        Pylon::PylonTerminate();
    }

    qDebug()<<"Terminate";
}
