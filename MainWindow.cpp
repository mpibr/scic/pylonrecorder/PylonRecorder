#include "MainWindow.h"
#include "ui_MainWindow.h"

Q_DECLARE_METATYPE(cv::Mat)

MainWindow::MainWindow(Capture* capture, CommandLineOptions options, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug()<< "OpenCV version : " << CV_VERSION;

    // Additional event datatypes
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<cv::Size>("cv::Size");
    qRegisterMetaType<std::vector<double>>("std::vector<double>");

    ui->setupUi(this);
    QList<int> sizes;
    sizes << 500 << ui->horizontalLayout_right->sizeHint().height();
    ui->splitter_control->setSizes(sizes);

    this->capture = capture;

    this->setGeometry(options.wndX, options.wndY, options.wndW, options.wndH);

    cameraInfo = (capture->modelName + " @"
                  + QString::number(capture->frameRate) + "fps"
                  + " " + QString::number(capture->frameSize.width)
                  + "x" + QString::number(capture->frameSize.height));

    ui->statusBar->showMessage(cameraInfo);
    ui->statusBar->setToolTip("Current settings of the camera and status of acquisition");
    label_recorded = new QLabel(this);
    label_acquired = new QLabel(this);
    label_statusSeparator = new QLabel(this);
    label_recorded->setText("0");
    label_recorded->setToolTip("Frames written to disc");
    label_acquired->setText("0");
    label_acquired->setToolTip("Frames acquired");
    label_statusSeparator->setText(" / ");

    label_availableRAM = new QLabel(this);
    label_RAMSeparator = new QLabel(this);
    label_totalRAM = new QLabel(this);
    label_availableRAM->setToolTip("Available RAM");
    label_RAMSeparator->setText("/");
    label_totalRAM->setToolTip("Total RAM");
    label_totalRAM->setMinimumWidth(80);

    ui->statusBar->addPermanentWidget(label_availableRAM);
    ui->statusBar->addPermanentWidget(label_RAMSeparator);
    ui->statusBar->addPermanentWidget(label_totalRAM);

    ui->statusBar->addPermanentWidget(label_recorded);
    ui->statusBar->addPermanentWidget(label_statusSeparator);
    ui->statusBar->addPermanentWidget(label_acquired);

    if(options.displayFrameRate == 0){
        displayItv = 0;
    }else{
        displayItv = ceil(float(capture->frameRate) / float(options.displayFrameRate));
        if(displayItv < 1){
            displayItv = 1;
        }
    }

    qDebug()<<"Showing every " << displayItv << " frame (" << capture->frameRate  << "/" << options.displayFrameRate << ")";

    filePath = options.defaultPath;

    // Generate autofilename with camera prefix if camera specified
    if(options.cameraIdx>=0){
        cameraIdxString = "cam"+QString::number(options.cameraIdx).append("_");
        autoFileNameTemplate.prepend(cameraIdxString);
        autoTrackingFileName.prepend(cameraIdxString);
    }else{
        cameraIdxString = "";
    }

    // Initialize others
    socketServer = new SocketServer(this);
    recordingWorker = new RecordingWorker();
    recordingThread = new QThread(this);
    recordingWorker->codec = options.recordingCodec;
    recordingWorker->compression = options.compression;
    ramMonitorThread = new QThread(this);
    ramMonitor = new RamMonitor();
    label_totalRAM->setText(QString::number(ramMonitor->totalRAM));

    // About Dialog
    QAction* actionAbout = new QAction("&About",this);
    ui->mainToolBar->addAction(actionAbout);

    // Validators
    QIntValidator *intValidator = new QIntValidator(this);
    intValidator->setBottom(0); //maximum defaults to int max
    QRegExp longlongRegEx("[0-9]*");
    QRegExp aviFilenameRegEx("*.avi");
    aviFilenameRegEx.setPatternSyntax(QRegExp::Wildcard);
    QValidator* longlongValidator = new QRegExpValidator(longlongRegEx, this);
    QValidator* aviFileNameValidator = new QRegExpValidator(aviFilenameRegEx, this);
    ui->lineEdit_nFrames->setValidator(longlongValidator);
    ui->lineEdit_fileName->setValidator(aviFileNameValidator);
    ui->lineEdit_nFrames->setValidator(intValidator);

    // Connection Main - Socket
    connect(socketServer, SIGNAL(statusInfo(QString)), this, SLOT(onStatusInfo(QString)));
    connect(ui->pushButton_stop, SIGNAL(clicked(bool)), socketServer, SLOT(onStop()));
    connect(this, SIGNAL(openedFileName(QString)), socketServer, SLOT(onFileName(QString)));

    // Connection Main - Capture
    connect(capture, SIGNAL(statusInfo(QString)), this, SLOT(onStatusInfo(QString)));
    connect(capture, SIGNAL(sendFrame(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
    connect(capture, SIGNAL(startTimeStampEvent(quint64)), this, SLOT(onBurstStarted(quint64)));
    connect(capture, SIGNAL(stopTimeStampEvent(quint64)), this, SLOT(onBurstStopped(quint64)));

    // Connect external linestop signal (activated through command line args)
    if (options.stopLine != 0){
        capture->stopLine = options.stopLine;
        connect(capture, SIGNAL(lineInputEvent(bool)), this, SLOT(onUserInput(bool)));
    }

    // Connection Main - Recorder
    connect(recordingWorker, SIGNAL(statusInfo(QString)), this, SLOT(onStatusInfo(QString)));
    connect(recordingWorker, SIGNAL(frameWritten(long long)), this, SLOT(onFrameWritten(long long)));
    connect(recordingWorker, SIGNAL(allWritten()),this, SLOT(onAllWritten()));
    connect(this, SIGNAL(videoOpen(QString, int, cv::Size)), recordingWorker, SLOT(onVideoOpen(QString, int, cv::Size)));
    connect(this, SIGNAL(videoClose()), recordingWorker, SLOT(onVideoClose()));
    connect(this, SIGNAL(setNImagesToRecord(long long)), recordingWorker, SLOT(onSetNImagesToGrab(long long)));

    // Connection Main - About
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(onAbout()));

    // Connection Capture - Socket
    connect(capture, SIGNAL(timeStampEvent(quint64)), socketServer, SLOT(onTimeStamp(quint64)));
    connect(capture, SIGNAL(startTimeStampEvent(quint64)), socketServer, SLOT(onStartTimeStamp(quint64)));
    connect(capture, SIGNAL(stopTimeStampEvent(quint64)), socketServer, SLOT(onStopTimeStamp(quint64)));

    // Connection Capture - Recorder
    connect(capture, SIGNAL(sendFrame(cv::Mat)), recordingWorker, SLOT(onFrameGrabbed(cv::Mat)));

    // Connection Main - RamMonitor
    connect(ramMonitor, SIGNAL(availableRAM(long)), this, SLOT(onAvailableRam(long)));

    // Move Recorder to separate Thread
    recordingWorker->moveToThread(recordingThread);
    connect(recordingThread, SIGNAL(finished()), recordingWorker, SLOT(deleteLater()));
    recordingThread->start();

    // Move Ram Monitor to separate Thread
    ramMonitor->moveToThread(ramMonitorThread);
    connect(ramMonitorThread, SIGNAL(finished()), ramMonitor, SLOT(deleteLater()));
    ramMonitorThread->start();

    scene = new QGraphicsScene(this);
    pixmapItem = new QGraphicsPixmapItem();

    scene->addItem(pixmapItem);

    // Initialize Tracking plugins
    QDir pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");

    ui->checkBox_trackingPreview->setVisible(false);
    ui->checkBox_recordTracking->setVisible(false);
    ui->label_trackingFileName->setVisible(false);
    ui->lineEdit_trackingFileName->setVisible(false);
    ui->pushButton_selectTrackingFile->setVisible(false);

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) {
            trackerInterface = qobject_cast <TrackerInterface* >(plugin);
            if(trackerInterface){ //Plugin successfully loaded
                trackingThread = new QThread(this);
                trackerRecordingWorker = new TrackerRecordingWorker(this);
                connect(capture, SIGNAL(sendFrame(cv::Mat)), trackerInterface->worker(), SLOT(onFrameGrabbed(cv::Mat)));
                connect(trackerInterface->worker(), SIGNAL(trackingResult(std::vector<double>)), socketServer, SLOT(onTrackingResult(std::vector<double>)));
                connect(trackerInterface->worker(), SIGNAL(trackingResult(std::vector<double>)), trackerRecordingWorker, SLOT(onTrackingResult(std::vector<double>)));
                connect(trackerInterface->worker(), SIGNAL(trackingMessage(std::string)), trackerRecordingWorker, SLOT(onTrackingMessage(std::string)));
                connect(trackerInterface->worker(), SIGNAL(userOutput(bool)), capture, SLOT(setUserOutput(bool)));
                connect(capture, SIGNAL(lineInputEvent(bool)), trackerInterface->worker(), SLOT(onUserInput(bool)));
                connect(this, SIGNAL(textFileOpen(QString)), trackerRecordingWorker, SLOT(onTextFileOpen(QString)));
                connect(this, SIGNAL(textFileClose()), trackerRecordingWorker, SLOT(onTextFileClose()));

                //connect(trackerInterface->worker(), SIGNAL(trackingResult(std::vector<double>)), ui->widget_trackingRecorder, SLOT(onWrite(std::vector<double>)));
                trackerInterface->worker()->moveToThread(trackingThread);
                trackerInterface->initializeUI(ui->verticalLayout_Plugin, scene, QSize(capture->frameSize.width, capture->frameSize.height));
                trackingThread->start();

                ui->checkBox_trackingPreview->setVisible(true);

                bool isLogFileRecording = trackerInterface->getIsLogFileRecording();

                // Temporarily disabled since most application store this data through socket
                ui->checkBox_recordTracking->setVisible(isLogFileRecording);
                ui->label_trackingFileName->setVisible(isLogFileRecording);
                ui->lineEdit_trackingFileName->setVisible(isLogFileRecording);
                ui->pushButton_selectTrackingFile->setVisible(isLogFileRecording);

                qDebug() << "Plugin: " << fileName << " loaded";
                break;
            }
        }else{
            qDebug()<< "Failed to load plugin";
        }
    }

    // Set more default GUI settings based on command line options
    if(options.autoFileName){
        ui->checkBox_autoFileName->setChecked(true);
        this->on_checkBox_autoFileName_clicked(true);
    }
    if(options.record){
        ui->checkBox_record->setChecked(true);
        this->on_checkBox_record_clicked(true);
    }
    if(options.external){
        ui->checkBox_externalTrigger->setChecked(true);
        this->on_checkBox_externalTrigger_clicked(true);
    }
    if(options.framesToCapture > 0){
        ui->lineEdit_nFrames->setText(QString::number(options.framesToCapture));
    }

    switch (options.captureMode){
    case CAPTURE_CONTINUOUS:
        ui->radioButton_continuous->setChecked(true);
        this->on_radioButton_continuous_clicked();
        break;
    case CAPTURE_BURST:
        ui->radioButton_burst->setChecked(true);
        this->on_radioButton_burst_clicked();
        break;
    case CAPTURE_FRAMEWISE:
        ui->radioButton_frameWise->setChecked(true);
        this->on_radioButton_frameWise_clicked();
        break;
    }

    QPixmap blackImage(capture->frameSize.width, capture->frameSize.height);
    blackImage.fill(Qt::black);
    pixmapItem->setPixmap(blackImage);
    ui->graphicsView_frame->setScene(scene);

    // Somehow forces the sceneRect to update
    ui->graphicsView_frame->setSceneRect(ui->graphicsView_frame->sceneRect());

    // Set external trigger to gui default
    capture->setExternalTrigger(ui->checkBox_externalTrigger->isChecked());

    // Start continuous acquisition (Live view)
    capture->softwareTrigger();
}

MainWindow::~MainWindow()
{
    if(recordingThread){
        recordingThread->quit();
        recordingThread->wait();
    }

    if(trackingThread){
        trackingThread->quit();
        trackingThread->wait();
        delete trackerInterface;
    }

    if(ramMonitorThread){
        ramMonitorThread->quit();
        ramMonitorThread->wait();
    }

    // delete recordingWorker;
    delete pixmapItem;
    delete ui;
}

// Convert openCV image to QImage
QImage MainWindow::putImage(const cv::Mat& mat)
{
    // 8-bits unsigned, NO. OF CHANNELS=1
    if(mat.type()==CV_8UC1){
        // Set the color table (used to translate colour indexes to qRgb values)
        QVector<QRgb> colorTable;
        for (int i=0; i<256; i++)
            colorTable.push_back(qRgb(i,i,i));
        // Copy input Mat
        const uchar *qImageBuffer = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, QImage::Format_Indexed8);
        img.setColorTable(colorTable);
        return img;
    }
    // 8-bits unsigned, NO. OF CHANNELS=3
    if(mat.type()==CV_8UC3){
        // Copy input Mat
        const uchar *qImageBuffer = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, QImage::Format_RGB888);
        return img.rgbSwapped();
    }else{
        qDebug() << "ERROR: Mat could not be converted to QImage.";
        return QImage();
    }
}

// Automatically connected UI Slots
void MainWindow::on_lineEdit_nFrames_editingFinished()
{
    // Set the number of frames that should be recorded/captured
    if(ui->lineEdit_nFrames->text().isEmpty()){
        nImagesToGrab = 0;
    }else{
        nImagesToGrab = ui->lineEdit_nFrames->text().toInt();
    }
    int burstTime = (1000*(nImagesToGrab) / capture->frameRate); //Burst time in seconds
    ui->label_burstTime->setText(" = " + TimeConversion::stringConvert(burstTime) + " [hh:mm:ss:ms]");
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_radioButton_continuous_clicked()
{
    ui->lineEdit_nFrames->setEnabled(false);
    ui->label_burstTime->setText("");
    nImagesToGrab = 0;
    captureMode = CAPTURE_CONTINUOUS;
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_radioButton_frameWise_clicked()
{
    ui->lineEdit_nFrames->setEnabled(false);
    ui->label_burstTime->setText("");
    nImagesToGrab = 0;
    captureMode = CAPTURE_FRAMEWISE;
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_radioButton_burst_clicked()
{
    ui->lineEdit_nFrames->setEnabled(true);
    // Set the number of frames that should be recorded/captured
    if(ui->lineEdit_nFrames->text().isEmpty()){
        nImagesToGrab = 0;
    }else{
        nImagesToGrab = ui->lineEdit_nFrames->text().toInt();
        int burstTime = (1000*(nImagesToGrab) / capture->frameRate); //Burst time in seconds
        ui->label_burstTime->setText(" = " + TimeConversion::stringConvert(burstTime) + " [hh:mm:ss:ms]");
    }
    captureMode = CAPTURE_BURST;
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_pushButton_softwareTrigger_clicked()
{
    if(isRecording & !wasWritten){
        QMessageBox msgBox;
        msgBox.setText("Unable to trigger recording. Program is still writing to harddrive\n");
        msgBox.exec();
    }else{
        capture->softwareTrigger();
    }
}

void MainWindow::on_pushButton_stop_clicked()
{
    // Assure that all grabbed frames are being written
    emit(setNImagesToRecord(grabbed));
    emit(videoClose());
    emit(textFileClose());
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_checkBox_externalTrigger_clicked(bool checked)
{
    // Optionally set external Trigger
    capture->setExternalTrigger(checked);
    capture->init(captureMode, nImagesToGrab);
}

void MainWindow::on_checkBox_record_clicked(bool checked)
{
    isRecording = checked;

    // Make sure that no files are still open
    if(!isRecording){
        emit(videoClose());
    }
}

void MainWindow::on_pushButton_selectFile_clicked()
{
    if(ui->checkBox_autoFileName->isChecked()){ // select folder only
        QString folderName = QFileDialog::getExistingDirectory(0, "Folder", filePath);

        if(!folderName.isEmpty()){
            filePath = folderName;
            fileName = filePath + "/" + autoFileNameTemplate;
            ui->lineEdit_fileName->setText(fileName);
        }

    }else{ // Select file and folder
        QString tmpFileName = QFileDialog::getSaveFileName(0, "Save file", filePath,
                                                           "Video files (*.avi)");
        if(!tmpFileName.isEmpty()){
            QFileInfo fileInfo(tmpFileName);
            filePath = fileInfo.absolutePath();
            lineEditFileName = fileInfo.fileName();
            fileName = tmpFileName;
            ui->lineEdit_fileName->setText(fileName);
        }
    }
}

void MainWindow::on_checkBox_autoFileName_clicked(bool checked)
{
    ui->lineEdit_fileName->setEnabled(!checked);
    ui->lineEdit_trackingFileName->setEnabled(!checked);

    if(checked){
        fileName = filePath + '/' + autoFileNameTemplate;
        trackingFileName = trackingFilePath + '/' + autoTrackingFileNameTemplate;
    }else{
        fileName = filePath + '/' + lineEditFileName;
        trackingFileName = trackingFilePath + '/' + lineEditTrackingFileName;
    }

    ui->lineEdit_fileName->setText(fileName);
    ui->lineEdit_trackingFileName->setText(trackingFileName);
}

void MainWindow::on_lineEdit_fileName_editingFinished()
{
    lineEditFileName = ui->lineEdit_fileName->text();
    QFileInfo fileInfo(lineEditFileName);
    lineEditFileName = fileInfo.fileName();
    filePath = fileInfo.absolutePath();
    fileName = filePath + '/' + lineEditFileName;
    qDebug()<<fileName;
}

void MainWindow::on_checkBox_trackingPreview_clicked(bool checked)
{
    if(checked){
        disconnect(capture, SIGNAL(sendFrame(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
        connect(trackerInterface->worker(), SIGNAL(trackingPreview(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
    }else{
        disconnect(trackerInterface->worker(), SIGNAL(trackingPreview(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
        connect(capture, SIGNAL(sendFrame(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
    }
}

void MainWindow::on_checkBox_recordTracking_clicked(bool checked)
{
    isTrackingRecording = checked;

    // Make sure that no files are still open
    if(!isTrackingRecording){
        emit(textFileClose());
    }
}

// Additional slots
void MainWindow::onBurstStarted(quint64 timeStamp)
{
    isStopped = false;

    // AUTOMATIC FILENAMES
    if(ui->checkBox_autoFileName->isChecked()){
        QString timeString = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
        autoFileName = cameraIdxString + timeString + ".avi";
        autoTrackingFileName = cameraIdxString + timeString + ".csv";
        fileName = filePath + "/" + autoFileName;
        trackingFileName = trackingFilePath + "/" + autoTrackingFileName;
        ui->lineEdit_fileName->setText(fileName);
        ui->lineEdit_trackingFileName->setText(trackingFileName);
    }

    // RECORDING
    if(ui->checkBox_record->isChecked()){
        emit(setNImagesToRecord(nImagesToGrab));
        emit(openedFileName(fileName));
        emit(textFileOpen(trackingFileName));
        emit(videoOpen(fileName, capture->frameRate, capture->frameSize));
        wasWritten = false;
        qDebug()<<"Main:Video file opened on first burst";
    }

    qDebug()<<"Main: Burst started " << timeStamp;

    // Reset recorded counter
    grabbed = 0;
    label_recorded->setText("0");
}

// Called when burst stops
void MainWindow::onBurstStopped(quint64 timeStamp)
{
    qDebug()<<"Burst stopped " << timeStamp;
    emit(videoClose());
    emit(textFileClose());
    capture->init(captureMode, nImagesToGrab);
    isStopped = true;
}

// Called each time a frame was record to update GUI text
void MainWindow::onFrameWritten(long long frameIdx)
{
    label_recorded->setText(QString::number(frameIdx));
}

void MainWindow::onAllWritten()
{
    wasWritten = true;
}

void MainWindow::onAvailableRam(long availableRam)
{
    label_availableRAM->setText(QString::number(availableRam));
}
// Called each time a frame is grabbed
void MainWindow::onFrameGrabbed(cv::Mat im)
{
    grabbed++;
    if(displayItv>0){
        if((isStopped) | (grabbed % displayItv==0)){
            pixmapItem->setPixmap(QPixmap::fromImage(putImage(im)));
        }
    }
    label_acquired->setText(QString::number(grabbed));
}

// Called on optional TTL input to stop recording
void MainWindow::onUserInput(bool value)
{
    isRecording = value;
    ui->checkBox_record->setChecked(isRecording);

    // Make sure that no files are still open
    if(!isRecording){
        emit(videoClose());
    }
}

// Incoming status message
void MainWindow::onStatusInfo(QString info)
{
    ui->statusBar->showMessage(cameraInfo + " Last action: " + info);
}

// About dialog selected
void MainWindow::onAbout()
{
    AboutDialog* aboutDialog = new AboutDialog(this);
    aboutDialog->setAttribute(Qt::WA_DeleteOnClose);
    aboutDialog->show();
}

// Dynamic resizing of image based on window size
void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    ui->graphicsView_frame->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
}

// Dynamic resizing of image based on window size
void MainWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    ui->graphicsView_frame->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
}

void MainWindow::on_pushButton_selectTrackingFile_clicked()
{
    if(ui->checkBox_autoFileName->isChecked()){ //select folder only
        QString folderName = QFileDialog::getExistingDirectory(0, "Folder", trackingFilePath);
        if(!folderName.isEmpty()){
            trackingFilePath = folderName;
            trackingFileName = trackingFilePath + "/" + autoTrackingFileNameTemplate;
            ui->lineEdit_trackingFileName->setText(trackingFileName);
        }

    }else{ // Select file and folder
        QString tmpFileName = QFileDialog::getSaveFileName(0, "Save file", trackingFilePath,
                                                           "Text files (*.csv *.txt)");
        if(!tmpFileName.isEmpty()){
            QFileInfo fileInfo(tmpFileName);
            trackingFilePath = fileInfo.absolutePath();
            lineEditTrackingFileName = fileInfo.fileName();
            trackingFileName = tmpFileName;
            ui->lineEdit_trackingFileName->setText(trackingFileName);
        }
    }
}

void MainWindow::on_lineEdit_trackingFileName_editingFinished()
{
    lineEditTrackingFileName = ui->lineEdit_trackingFileName->text();
    QFileInfo fileInfo(lineEditTrackingFileName);
    lineEditTrackingFileName = fileInfo.fileName();
    trackingFilePath = fileInfo.absolutePath();
    trackingFileName = trackingFilePath + '/' + lineEditTrackingFileName;
    qDebug()<<trackingFileName;
}
