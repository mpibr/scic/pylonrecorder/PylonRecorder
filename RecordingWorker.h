// Deals with opening, closing and writing video files

#ifndef RECORDINGWORKER_H
#define RECORDINGWORKER_H
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QApplication> // To process event queue
#include <opencv2/highgui/highgui.hpp>
#include <QStorageInfo>
#include <QMessageBox>

class RecordingWorker : public QObject
{
    Q_OBJECT
public:
    explicit RecordingWorker(QObject* parent = 0);
    int codec = CV_FOURCC('X','2','6','4');
    float compression;

signals:
    void frameWritten(long long);
    void allWritten();
    void statusInfo(QString info);

public slots:
    void onFrameGrabbed(cv::Mat);
    void onVideoOpen(QString fileName, int frameRate, cv::Size frameSize);
    void onVideoClose();
    void onSetNImagesToGrab(long long);


private:
    cv::VideoWriter videoWriter;
    long long nImagesToGrab = 0;
    long long nImagesGrabbed = 0;
    const int DiskFullWarningLimit = 1000; // Emit warning when less than this MB is available
};

#endif // RECORDINGWORKER_H
