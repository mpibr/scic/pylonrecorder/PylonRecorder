//This is the main interface to the Pylon SDK

#ifndef CAPTURE_H
#define CAPTURE_H
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <QFileInfo>
#include <opencv2/core/core.hpp>
#include <pylon/PylonIncludes.h>
#include <pylon/InstantCamera.h>

#include "CaptureImageEventHandler.h"
#include "CaptureCameraEventHandler.h"
#include "CAcquireBurstSoftwareTriggerConfiguration.h"
#include "CaptureEvents.h"

enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

class Capture : public QObject
{
    Q_OBJECT

public:
    QString modelName;
    cv::Size frameSize;
    int frameRate = 0;
    bool isTriggered = false;
    bool isAvailable = false;
    unsigned int triggerLine = 0;
    unsigned int stopLine = 0;

    explicit inline Capture(QObject *parent = 0): QObject(parent){}

    virtual void init(int captureMode, long long nImagesToCapture) = 0;
    virtual void softwareTrigger() = 0;
    virtual void setFrameRate(int) = 0;
    virtual void setTriggered(bool) = 0;
    virtual void setExternalTrigger(bool) = 0;

signals:
    void sendFrame(cv::Mat);
    void setImagesToCapture(long long);
    void setCaptureMode(int captureMode);
    void timeStampEvent(quint64);
    void startTimeStampEvent(quint64);
    void stopTimeStampEvent(quint64);
    void statusInfo(QString info);
    void resetCounter();
    void lineInputEvent(bool isHigh);

public slots:
    virtual void setUserOutput(bool) = 0;

};

#endif // CAPTURE_H
