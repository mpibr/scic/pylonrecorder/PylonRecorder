//This is the main interface to the Pylon SDK

#ifndef PYLONCAPTURE_H
#define PYLONCAPTURE_H
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <QFileInfo>
#include <opencv2/core/core.hpp>
#include <pylon/PylonIncludes.h>
#include <pylon/InstantCamera.h>
#include "Capture.h"
#include "CaptureImageEventHandler.h"
#include "CaptureCameraEventHandler.h"
#include "CAcquireBurstSoftwareTriggerConfiguration.h"
#include "CaptureEvents.h"

class PylonCapture : public Capture
{
    Q_OBJECT

public:
    explicit PylonCapture(QObject *parent = 0, int cameraIdx = 0, QString configFile = "");
    ~PylonCapture();
    virtual void init(int captureMode, long long nImagesToCapture);
    virtual void softwareTrigger();
    virtual void setFrameRate(int);
    virtual void setTriggered(bool);
    virtual void setExternalTrigger(bool);
    virtual void setUserOutput(bool);
private:
    // Automagically call Pylonlnitialize and PylonTerminate to ensure the pylon runtime system
    // is initialized during the lifetime of this object.
    Pylon::PylonAutoInitTerm autoInitTerm;

    // Pylon devices
    Pylon::IPylonDevice *pDevice = NULL;
    Pylon::CInstantCamera *camera = NULL;

    CaptureImageEventHandler* cImageEventGrabber;
    CaptureCameraEventHandler* cCameraEventHandler;

    GenICam::gcstring stopLineString;
    GenICam::gcstring outputString;
    GenICam::gcstring triggerLineString;
};

#endif // PYLONCAPTURE_H
