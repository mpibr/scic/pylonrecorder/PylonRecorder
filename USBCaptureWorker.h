#ifndef USBCAPTUREWORKER_H
#define USBCAPTUREWORKER_H
#include <opencv2/highgui/highgui.hpp>
#include <QApplication>
#include <QObject>
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QTime>
#include <QApplication> // To process event queue

class USBCaptureWorker : public QObject
{
    Q_OBJECT
public:
    explicit USBCaptureWorker(cv::VideoCapture* = 0);
    cv::VideoCapture *cap;
    int frameRate = 10;
    bool running = false;
    QTime qTime;

public slots:
    void onCaptureNImages(long long nImages);
    void onSetFrameRate(int frameRate);
    void onStopGrabbing();

signals:
    void sendFrame(cv::Mat);
    void startTimeStampEvent(quint64);
    void stopTimeStampEvent(quint64);
};

#endif // USBCAPTUREWORKER_H
